drop table if exists assets;
create table assets (
  id          serial      primary key,
  hash        char(66)    unique not null,
  name        text        not null,
  symbol      text        not null,
  qty         text        not null,
  precision   numeric     not null,
  issuer      char(42)    not null,
  issue_time  int         not null,
  issue_tx    char(66)    not null,
  canchange   boolean,
  verified    boolean     default false,
  holders     int         default 0,
  txs         int         default 0,
  description json        default null
);

drop table if exists erc20;
create table erc20 (
  id            serial      primary key,
  hash          char(42)    unique not null,
  name          text        not null,
  symbol        text        not null,
  -- erc20 token can have a fixed or a variable supply
  qty          text        not null,
  precision     int         not null,
  verfied       boolean     default false,
  holders       int         default 0,
  txs           int         default 0, 
  -- for future usage
  info          json        default null
);


drop table if exists address;

create table address (
  id         serial       primary key,
  hash       char(42)     unique not null,
  label      text         default null,
  miner      boolean      default false,
  contract   boolean      default false,
  erc20      boolean      default false,
  exchange   boolean      default false,
  -- verfied contract, for future usage
  verfied    boolean      default false,
  usan       text         default null,
  create_at  int          default null,
  active_at  int          default null,
  txs        int          default 0,
  info       json         default null
);

create index  idx_address_hash
on address using hash(hash);
create index idx_address_usan
on address using hash(hash);


drop table if exists address_assets;
create table address_assets(
  asset                 char(66),
  address               char(42),
  qty                   numeric      default 0,
  qty_in                numeric      default 0,
  qty_own               numeric      default 0,
  constraint address_assets_pkey primary key(address, asset)
);

drop table if exists address_tl_assets;
create table address_tl_assets (
  asset                char(66),
  address              char(42),
  data                 json         not null,
  constraint address_tl_assets_pkey primary key(address, asset)
);

drop table if exists address_erc20_assets;
create table address_erc20_assets(
  asset   char(42)  not null,
  address char(42)  not null,
  qty     numeric   not null,
  constraint address_erc20_assets_pkey primary key(address, asset)
);

drop table if exists txs;
create table txs (
  id         bigserial   primary key,
  hash       char(66)    unique not null,
  -- 1: success, 0: failed
  status     int         not null,
  block      bigint      not null,
  fee        text        not null,
  type       int         not null,
  sender     char(42)    not null,
  receiver   char(42)    not null,
  assets     jsonb       default null,
  data       json        default null,
  timestamp  int         not null
);

create index idx_txs_sender   on txs(sender);
create index idx_txs_receiver on txs(receiver);
create index idex_assets 
on txs using hash(assets);

drop table if exists erc20_txs;
create table erc20_txs (
  id         bigserial   primary key,
  hash       char(66)    unique not null,
  -- 1: success, 0: failed
  status     int         not null,
  block      bigint      not null,
  fee        text        not null,
  type       int         not null,
  sender     char(42)    not null,
  receiver   char(42)    not null,
  assets     jsonb       default null,
  data       json        default null,
  timestamp  int         not null
);

-- FSN Token
insert into assets(hash, name, symbol, qty, precision, issuer, issue_time, issue_tx, canchange, verified)
values('0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff', 'FUSION', 'FSN', 81920000, 18, '0x0000000000000000000000000000000000000000', 1561894626,'0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff', false, true )