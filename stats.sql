drop table if exists txs_stats;
create table txs_stats (
  id        serial primary key,
  stats_at  int    unique not null,
  stats     json  not null 
);;